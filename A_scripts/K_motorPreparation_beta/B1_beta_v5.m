% check mu-beta dynamics as an indicator of motor preparation

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
pn.savePath     = [pn.root, 'B_analyses/S2_TFR/B_data/'];
pn.plotFolder   = [pn.root, 'B_analyses/S2_TFR/C_figures/'];

addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

    %% save TrlInfo from FT
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    %% CSD transform
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% wavelet transform (7 cycles): alpha-beta motor

    cfg                 = [];
    cfg.channel         = 'all';
    cfg.method          = 'wavelet';
    cfg.width           = 7;
    cfg.keeptrials      = 'yes';
    cfg.output          = 'pow';
    cfg.foi             = 8:1:25;
    cfg.toi             = 2:0.05:9.5; % 20 Hz
    TFRdata_AlphaBeta   = ft_freqanalysis(cfg, data);
    
    %% re-segment data with respect to probe onset & response 
    
    % baseline: average of -600 to -500 ms prior to probe onset
    
%     cfg = [];
%     cfg.baseline = [5.4 5.5];
%     [data] = ft_timelockbaseline(cfg, data);
%     [TFRdata_AlphaBeta] = ft_freqbaseline(cfg, TFRdata_AlphaBeta);
    
%        figure; imagesc(data.time,[],squeeze(nanmean(data.trial(TrlInfo(:,8)==4,:,:,:),1)-nanmean(data.trial(TrlInfo(:,8)==1,:,:,:),1)))
% 
%         figure; imagesc(dataProbe_AlphaBeta.time,[],squeeze(nanmean(dataProbe_AlphaBeta.powspctrm(TrlInfo(:,8)==1,27,:,:),1)))
%         figure; imagesc(dataProbe_AlphaBeta.time,[],squeeze(nanmean(dataProbe_AlphaBeta.powspctrm(TrlInfo(:,8)==4,27,:,:),1)))
%         figure; imagesc(dataResponse_AlphaBeta.time,[],squeeze(nanmean(dataResponse_AlphaBeta.powspctrm(TrlInfo(:,8)==4,27,:,:),1)))
%         figure; imagesc(dataResponse_AlphaBeta.time,[],squeeze(nanmean(dataResponse_AlphaBeta.powspctrm(TrlInfo(:,8)==1,27,:,:),1)))
%         
%         figure; imagesc(TFRdata_SSVEP.time,[],squeeze(nanmean(TFRdata_AlphaBeta.powspctrm(TrlInfo(:,8)==1,27,:,:),1)))
%         figure; imagesc(TFRdata_SSVEP.time,[],squeeze(nanmean(TFRdata_AlphaBeta.powspctrm(TrlInfo(:,8)==4,31,:,:)-...
%             TFRdata_AlphaBeta.powspctrm(TrlInfo(:,8)==4,27,:,:),1)))
%   figure; imagesc(dataProbe_AlphaBeta.time,[],squeeze(nanmean(dataProbe_AlphaBeta.powspctrm(TrlInfo(:,8)==4,31,:,:)-...
%             dataProbe_AlphaBeta.powspctrm(TrlInfo(:,8)==4,27,:,:),1)))
%       
%         figure; imagesc(dataResponse_AlphaBeta.time,[],squeeze(nanmean(dataResponse_AlphaBeta.powspctrm(TrlInfo(:,8)==2,31,:,:)-...
%             dataResponse_AlphaBeta.powspctrm(TrlInfo(:,8)==2,27,:,:),1)))
%         
%            figure; imagesc(dataResponse_AlphaBeta.time,[],squeeze(nanmean(dataResponse_AlphaBeta.powspctrm(TrlInfo(:,8)==1,31,:,:)-...
%             dataResponse_AlphaBeta.powspctrm(TrlInfo(:,8)==1,27,:,:),1)))

    % stim onset
    timepoint = repmat(3, size(TFRdata_AlphaBeta.powspctrm,1),1);
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-20; % now 20 Hz
    cfg.endsample = idx+20;
    dataStim_AlphaBeta = TFRdata_AlphaBeta;
    dataStim_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        dataStim_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataStim_AlphaBeta.time = -20*50:50:20*50;
   
    % probe onset
    
    timepoint = repmat(6.04, size(data.trial,2),1);
    time = repmat(data.time{1}, size(data.trial,2),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-400;
    cfg.endsample = idx+400;
    dataProbe = data;
    dataProbe.trial = NaN(size(data.trial,2),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        dataProbe.trial(indTrial,:,:) = data.trial{indTrial}(:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataProbe.time = -400*2:2:400*2;
    
    timepoint = repmat(6.04, size(TFRdata_AlphaBeta.powspctrm,1),1);
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-20; % now 20 Hz
    cfg.endsample = idx+20;
    dataProbe_AlphaBeta = TFRdata_AlphaBeta;
    dataProbe_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        dataProbe_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataProbe_AlphaBeta.time = -20*50:50:20*50;
    
    % response
    
    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(data.time{1},2));
    time = repmat(data.time{1}, size(data.trial,2),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-400;
    cfg.endsample = idx+400;
    trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(data.trial{1},2));
    dataResponse = data;
    dataResponse.trial = NaN(size(data.trial,2),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        if ismember(indTrial, trialToRemove)
            dataResponse.trial(indTrial,:,:) = NaN;
        else
        	dataResponse.trial(indTrial,:,:) = data.trial{indTrial}(:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse.time = -400*2:2:400*2;
    
    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(TFRdata_AlphaBeta.powspctrm,4));   
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-40; % now 20 Hz
    cfg.endsample = idx+20;
    trialToRemove = [trialToRemove; find(cfg.begsample<=0 | cfg.endsample>size(TFRdata_AlphaBeta.powspctrm,4))];
    dataResponse_AlphaBeta = TFRdata_AlphaBeta;
    dataResponse_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        if ismember(indTrial, trialToRemove)
            dataResponse_AlphaBeta.powspctrm(indTrial,:,:,:) = NaN;
        else
            dataResponse_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse_AlphaBeta.time = [-40*.05:.05:20*.05].*1000; % in ms
    
    % remove empty trials from all sturctures
    
    dataProbe.trial(trialToRemove,:,:) = []; dataProbe.trialToRemove = numel(trialToRemove);
    dataProbe_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; dataProbe_AlphaBeta.trialToRemove = numel(trialToRemove);
    
    dataResponse.trial(trialToRemove,:,:) = []; dataResponse.trialToRemove = numel(trialToRemove);
    dataResponse_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; dataResponse_AlphaBeta.trialToRemove = numel(trialToRemove);
    
    TrlInfo(trialToRemove,:) = [];
    
    disp(['Removed trials: ', num2str(numel(trialToRemove))]);
    
    %% split into left vs. right response & condition
    % do this for both TFR and time domain data
    
    % identify left/right response for each trial
    behavData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat');
    leftRightVector = behavData.MergedDataEEG.TargetOption(TrlInfo(:,7),... % 1 = left; 2 = right
        find(strcmp(behavData.IDs_all, IDs{id})));
    
    Accs = behavData.MergedDataEEG.Accs(TrlInfo(:,7),find(strcmp(behavData.IDs_all, IDs{id})));
    
    % determine which response was given
    for indTrial = 1:numel(leftRightVector)
        if leftRightVector(indTrial) == 1 & Accs(indTrial) == 1 % correct resp was left, correct resp, resp = left
            leftRightVector(indTrial) = 1;
        elseif leftRightVector(indTrial) == 1 & Accs(indTrial) == 0 % correct resp was left, incorrect resp, resp = right
            leftRightVector(indTrial) = 2;
        elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 1 % correct resp was right, correct resp, resp = right
            leftRightVector(indTrial) = 2;
        elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 0 % correct resp was right, incorrect resp, resp = left
            leftRightVector(indTrial) = 1;
        end
    end
    
    % flip left and right channels in layout
    
%     chanLocs = cellfun(@(x) x(end), data.label,'UniformOutput',false);
%     chanLocs(strcmp(chanLocs, {'z'})) = {'NaN'};
%     chanLocs = cellfun(@str2num,chanLocs,'un',0);
%     chanLocs = cell2mat(chanLocs);
% 
%     odd = mod(chanLocs,2) == 1; % left EEG channels
%     even = mod(chanLocs,2) == 0; % right EEG channels
    
    labels_odd = {'Fp1';'AF7';'AF3';'F7';'F5';'F3';'F1';'FT7';'FC5';'FC3';'FC1';'T7';'C5';'C3';'C1';'TP7';'CP5';'CP3';'CP1';'P7';'P5';'P3';'PO9';'PO7';'PO3';'O1'};    
    labels_even = {'Fp2';'AF8';'AF4';'F8';'F6';'F4';'F2';'FT8';'FC6';'FC4';'FC2';'T8';'C6';'C4';'C2';'TP8';'CP6';'CP4';'CP2';'P8';'P6';'P4';'PO10';'PO8';'PO4';'O2'};
    
    odd = find(ismember(data.label, labels_odd));
    even = find(ismember(data.label, labels_even));
    
    %[labels_odd, labels_even] % check that labels are corresponding!
    
    % for left responses, switch left and right channels, i.e. left is ALWAYS contralateral
    % left response: left channels represent right channels (contralateral response)
    % left response: right channels represent left channels (ipsilateral response)
    % right response: original layout (i.e., contralateral = left)
    
    curBackup = dataStim_AlphaBeta.powspctrm;
    dataStim_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataStim_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    curBackup = dataProbe.trial;
    dataProbe.trial(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataProbe.trial(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);

    curBackup = dataProbe_AlphaBeta.powspctrm;
    dataProbe_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataProbe_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    curBackup = dataResponse.trial;
    dataResponse.trial(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataResponse.trial(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    curBackup = dataResponse_AlphaBeta.powspctrm;
    dataResponse_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataResponse_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    % calculate single-trial power difference left vs. right
    % left channels represent contra-ipsi; right channels represent ipsi-contra 
    
    dataStim_AlphaBetaDiff = dataStim_AlphaBeta;
    dataStim_AlphaBetaDiff.powspctrm(:,even,:) = dataStim_AlphaBeta.powspctrm(:,even,:) - dataStim_AlphaBeta.powspctrm(:,odd,:);
    dataStim_AlphaBetaDiff.powspctrm(:,odd,:) = dataStim_AlphaBeta.powspctrm(:,odd,:) - dataStim_AlphaBeta.powspctrm(:,even,:);
    
    dataProbeDiff = dataProbe;
    dataProbeDiff.trial(:,even,:) = dataProbe.trial(:,even,:) - dataProbe.trial(:,odd,:);
    dataProbeDiff.trial(:,odd,:) = dataProbe.trial(:,odd,:) - dataProbe.trial(:,even,:);
    
    dataProbe_AlphaBetaDiff = dataProbe_AlphaBeta;
    dataProbe_AlphaBetaDiff.powspctrm(:,even,:) = dataProbe_AlphaBeta.powspctrm(:,even,:) - dataProbe_AlphaBeta.powspctrm(:,odd,:);
    dataProbe_AlphaBetaDiff.powspctrm(:,odd,:) = dataProbe_AlphaBeta.powspctrm(:,odd,:) - dataProbe_AlphaBeta.powspctrm(:,even,:);
    
    dataResponseDiff = dataResponse;
    dataResponseDiff.trial(:,even,:) = dataResponse.trial(:,even,:) - dataResponse.trial(:,odd,:);
    dataResponseDiff.trial(:,odd,:) = dataResponse.trial(:,odd,:) - dataResponse.trial(:,even,:);
    
    dataResponse_AlphaBetaDiff = dataResponse_AlphaBeta;
    dataResponse_AlphaBetaDiff.powspctrm(:,even,:) = dataResponse_AlphaBeta.powspctrm(:,even,:) - dataResponse_AlphaBeta.powspctrm(:,odd,:);
    dataResponse_AlphaBetaDiff.powspctrm(:,odd,:) = dataResponse_AlphaBeta.powspctrm(:,odd,:) - dataResponse_AlphaBeta.powspctrm(:,even,:);
    
    % average within condition
    
    for indCond = 1:4
        cfg = [];
        cfg.trials = TrlInfo(:,8)==indCond;
        dataStimAvg_AlphaBeta{indCond} = dataStim_AlphaBeta; % Hack for data including multiple freqs, which get concatenated by FT
        dataStimAvg_AlphaBeta{indCond}.powspctrm = ...
            squeeze(nanmean(dataStimAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataStimAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
        % timelock to probe onset
        dataProbeAvg{indCond} = ft_timelockanalysis(cfg, dataProbe);
        dataProbeAvg_AlphaBeta{indCond} = dataProbe_AlphaBeta; % Hack for data including multiple freqs, which get concatenated by FT
        dataProbeAvg_AlphaBeta{indCond}.powspctrm = ...
            squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataProbeAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
        % timelock to response
        dataResponseAvg{indCond} = ft_timelockanalysis(cfg, dataResponse);
        dataResponseAvg_AlphaBeta{indCond} = dataResponse_AlphaBeta;
        dataResponseAvg_AlphaBeta{indCond}.powspctrm = ...
            squeeze(nanmean(dataResponseAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataResponseAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
        % diff versions
        dataStimAvg_AlphaBetaDiff{indCond} = dataStim_AlphaBetaDiff; % Hack for data including multiple freqs, which get concatenated by FT
        dataStimAvg_AlphaBetaDiff{indCond}.powspctrm = ...
            squeeze(nanmean(dataStimAvg_AlphaBetaDiff{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataStimAvg_AlphaBetaDiff{indCond}.dimord = 'chan_freq_time';
        dataProbeAvgDiff{indCond} = ft_timelockanalysis(cfg, dataProbeDiff);
        dataProbeAvg_AlphaBetaDiff{indCond} = dataProbe_AlphaBetaDiff; % Hack for data including multiple freqs, which get concatenated by FT
        dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm = ...
            squeeze(nanmean(dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataProbeAvg_AlphaBetaDiff{indCond}.dimord = 'chan_freq_time';
        dataResponseAvgDiff{indCond} = ft_timelockanalysis(cfg, dataResponseDiff);
        dataResponseAvg_AlphaBetaDiff{indCond} = dataResponse_AlphaBetaDiff;
        dataResponseAvg_AlphaBetaDiff{indCond}.powspctrm = ...
            squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataResponseAvg_AlphaBetaDiff{indCond}.dimord = 'chan_freq_time';
    end
    
    %% save computed files

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/K_motor/';
    save([pn.out, IDs{id}, '_motor_v5.mat'], 'dataStimAvg_AlphaBeta','dataProbeAvg', 'dataProbeAvg_AlphaBeta',...
        'dataResponseAvg', 'dataResponseAvg_AlphaBeta',...
        'dataStimAvg_AlphaBetaDiff','dataProbeAvgDiff', 'dataProbeAvg_AlphaBetaDiff',...
        'dataResponseAvgDiff', 'dataResponseAvg_AlphaBetaDiff');

    clear data*
    
end % ID
