% check mu-beta dynamics as an indicator of motor preparation

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
%     '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
%     '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
%     '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
%     '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
%     '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
%     '2252';'2258';'2261'};

%% collect all subjects

ERPstruct = [];
for id = 1:length(IDs)
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/K_motor/';
    load([pn.out, IDs{id}, '_motor_v5.mat'], 'dataProbeAvg', 'dataProbeAvg_AlphaBeta', 'dataProbeAvgDiff', 'dataProbeAvg_AlphaBetaDiff',...
        'dataResponseAvg', 'dataResponseAvg_AlphaBeta', 'dataResponseAvgDiff','dataResponseAvg_AlphaBetaDiff');
    ERPstruct.dataProbeAvg(:,id) = dataProbeAvg;
    ERPstruct.dataProbeAvgDiff(:,id) = dataProbeAvgDiff;
    blTime = [-200 0];
    freqs = dataProbeAvg_AlphaBeta{1,1}.freq > 8 & dataProbeAvg_AlphaBeta{1,1}.freq < 25;
    % average across alpha-beta freqs
    for indCond = 1:4
        % baseline-correct prior to frequency average: condition-specific baseline
        curTime = dataProbeAvg_AlphaBeta{indCond}.time;
        dataProbeAvg_AlphaBeta{indCond}.powspctrm = dataProbeAvg_AlphaBeta{indCond}.powspctrm-...
            repmat(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(:,:,curTime>blTime(1)&curTime<blTime(2)),3),1,1,numel(curTime));
        dataProbeAvg_AlphaBeta{indCond}.avg = squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(:,freqs,:),2));
        dataProbeAvg_AlphaBeta{indCond}.dimord = 'chan_time';
%         dataProbeAvg_AlphaBeta{indCond} = ...
%             rmfield(dataProbeAvg_AlphaBeta{indCond}, 'powspctrm');
        dataProbeAvg_AlphaBeta{indCond} = ...
            rmfield(dataProbeAvg_AlphaBeta{indCond}, 'freq');
        
        curTime = dataProbeAvg_AlphaBetaDiff{indCond}.time;
        dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm = dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm-...
            repmat(nanmean(dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm(:,:,curTime>blTime(1)&curTime<blTime(2)),3),1,1,numel(curTime));
        dataProbeAvg_AlphaBetaDiff{indCond}.avg = squeeze(nanmean(dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm(:,freqs,:),2));
        dataProbeAvg_AlphaBetaDiff{indCond}.dimord = 'chan_time';
%         dataProbeAvg_AlphaBetaDiff{indCond} = ...
%             rmfield(dataProbeAvg_AlphaBetaDiff{indCond}, 'powspctrm');
        dataProbeAvg_AlphaBetaDiff{indCond} = ...
            rmfield(dataProbeAvg_AlphaBetaDiff{indCond}, 'freq');
    end
    ERPstruct.dataProbeAvg_AlphaBeta(:,id) = dataProbeAvg_AlphaBeta;
    ERPstruct.dataProbeAvg_AlphaBetaDiff(:,id) = dataProbeAvg_AlphaBetaDiff;
    ERPstruct.dataResponseAvg(:,id) = dataResponseAvg;
    ERPstruct.dataResponseAvgDiff(:,id) = dataResponseAvgDiff;
    % average across alpha-beta freqs
    for indCond = 1:4
        curTime = dataProbeAvg_AlphaBeta{indCond}.time;
        dataResponseAvg_AlphaBeta{indCond}.powspctrm = dataResponseAvg_AlphaBeta{indCond}.powspctrm-...
            repmat(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(:,:,curTime>blTime(1)&curTime<blTime(2)),3),1,1,numel(curTime));
        dataResponseAvg_AlphaBeta{indCond}.avg = squeeze(nanmean(dataResponseAvg_AlphaBeta{indCond}.powspctrm(:,freqs,:),2));
        dataResponseAvg_AlphaBeta{indCond}.dimord = 'chan_time';
        dataResponseAvg_AlphaBeta{indCond} = ...
            rmfield(dataResponseAvg_AlphaBeta{indCond}, 'powspctrm');
        dataResponseAvg_AlphaBeta{indCond} = ...
            rmfield(dataResponseAvg_AlphaBeta{indCond}, 'freq');
        
        curTime = dataProbeAvg_AlphaBetaDiff{indCond}.time;
        dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm = dataResponseAvg_AlphaBetaDiff{indCond}.powspctrm-...
            repmat(nanmean(dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm(:,:,curTime>blTime(1)&curTime<blTime(2)),3),1,1,numel(curTime));
        dataResponseAvg_AlphaBetaDiff{indCond}.avg = squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{indCond}.powspctrm(:,freqs,:),2));
        dataResponseAvg_AlphaBetaDiff{indCond}.dimord = 'chan_time';
        dataResponseAvg_AlphaBetaDiff{indCond} = ...
            rmfield(dataResponseAvg_AlphaBetaDiff{indCond}, 'powspctrm');
        dataResponseAvg_AlphaBetaDiff{indCond} = ...
            rmfield(dataResponseAvg_AlphaBetaDiff{indCond}, 'freq');
        
        % remove remaining .powspctrm fields
        dataProbeAvg_AlphaBeta{indCond} = ...
            rmfield(dataProbeAvg_AlphaBeta{indCond}, 'powspctrm');
        dataProbeAvg_AlphaBetaDiff{indCond} = ...
            rmfield(dataProbeAvg_AlphaBetaDiff{indCond}, 'powspctrm');
    end
    ERPstruct.dataResponseAvg_AlphaBeta(:,id) = dataResponseAvg_AlphaBeta;
    ERPstruct.dataResponseAvg_AlphaBetaDiff(:,id) = dataResponseAvg_AlphaBetaDiff;
end

clear dataProbeAvg_AlphaBeta dataProbeAvg_AlphaBetaDiff dataResponseAvg_AlphaBeta dataResponseAvg_AlphaBetaDiff;

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

%% create group averages

cfg = [];
for indAge = 1%:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        dataProbeAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg{indCond,ageIdx{indAge}});
        dataProbeAvg_AlphaBeta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg_AlphaBeta{indCond,ageIdx{indAge}});
        dataResponseAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg{indCond,ageIdx{indAge}});
        dataResponseAvg_AlphaBeta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg_AlphaBeta{indCond,ageIdx{indAge}});
        dataProbeAvgDiff{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvgDiff{indCond,ageIdx{indAge}});
        dataProbeAvg_AlphaBetaDiff{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg_AlphaBetaDiff{indCond,ageIdx{indAge}});
        dataResponseAvgDiff{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvgDiff{indCond,ageIdx{indAge}});
        dataResponseAvg_AlphaBetaDiff{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg_AlphaBetaDiff{indCond,ageIdx{indAge}});
    end
end

%% Figure: 2D plots: channel x time

figure; 
subplot(2,6,1); imagesc(dataProbeAvg{1,1}.time, [], squeeze(nanmean(dataProbeAvg{1,1}.individual,1)));
subplot(2,6,3); imagesc(dataResponseAvg{1,1}.time, [], squeeze(nanmean(dataResponseAvg{1,1}.individual,1)));
subplot(2,6,5); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBeta{1,1}.individual,1)));
subplot(2,6,6); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBeta{4,1}.individual,1)));
subplot(2,6,6+1); imagesc(dataProbeAvg{1,1}.time, [], squeeze(nanmean(dataProbeAvgDiff{1,1}.individual,1)));
subplot(2,6,6+3); imagesc(dataResponseAvg{1,1}.time, [], squeeze(nanmean(dataResponseAvgDiff{1,1}.individual,1)));
subplot(2,6,6+5); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{1,1}.individual,1)));
subplot(2,6,6+6); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{4,1}.individual,1)));

%% define motor channels of interest

chans{1,1} = 'C4'; % right hemisphere: negative during left press (1)
chans{2,1} = 31;
chans{1,2} = 'C3'; % left hemisphere: negative during right press (2)
chans{2,2} = 27;

%% baseline correction: -200:0 on grand average beta series (condition-specific!)

dataProbeAvg_AlphaBeta_BL = []; dataResponseAvg_AlphaBeta_BL = []; dataResponseAvg_AlphaBetaDiff_BL =[];
idxBL = dataResponseAvg_AlphaBeta{1,1}.time >-200 & dataResponseAvg_AlphaBeta{1,1}.time<0;
for indAge = 1%:2
    for indCond = 1:4
        dataProbeAvg_AlphaBeta_BL{indCond,indAge} = dataProbeAvg_AlphaBeta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numel(idxBL));

        dataResponseAvg_AlphaBeta_BL{indCond,indAge} = dataResponseAvg_AlphaBeta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numel(idxBL));
        
        dataResponseAvg_AlphaBetaDiff_BL{indCond,indAge} = dataResponseAvg_AlphaBetaDiff{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataProbeAvg_AlphaBetaDiff{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numel(idxBL));

    end
end

%% Figure: plot beta power time series: subject x time

% indCond = 1;
% curData = squeeze(nanmean(cat(4, nanmean(dataResponseAvg_AlphaBeta_BL{indCond,1}(:,[31, 40],:),2), ...
%             nanmean(dataResponseAvg_AlphaBeta_BL{indCond,1}(:,[27, 36],:),2)),4));
% figure; imagesc(dataResponseAvg_AlphaBeta{1,1}.time,[],curData)

%% Figure: response-locked beta power time series

pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
            
    h = figure('units','normalized','position',[.1 .1 .4 .4]);
    % between-subject error bars
    % rearrange data in matrix
    for indCond = 1:4
        BLBeta(indCond,:,:) = squeeze(nanmean(dataResponseAvg_AlphaBeta_BL{indCond,1}(:,[27, 36],:),2));
        % BLBeta(indCond,:,:) = squeeze(nanmean(dataResponseAvg_AlphaBetaDiff_BL{indCond,1}(:,[27, 36],:),2));
    end
    cla; hold on;
    % between-subject error bars
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(BLBeta,1));
    curData = squeeze(BLBeta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .12],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .15],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .2],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(4,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .3],'linewidth', 2}, 'patchSaturation', .1);
    xlim([-1000 400]); xlabel('Time (ms); resp-locked')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral mu-beta relative to response')

%% Figure: probe-locked beta power time series
    
 h = figure('units','normalized','position',[.1 .1 .4 .4]);
    % between-subject error bars
    % rearrange data in matrix
    for indCond = 1:4
        BLBeta(indCond,:,:) = squeeze(nanmean(dataProbeAvg_AlphaBeta_BL{indCond,1}(:,[27, 36],:),2));
        % BLBeta(indCond,:,:) = squeeze(nanmean(dataResponseAvg_AlphaBetaDiff_BL{indCond,1}(:,[27, 36],:),2));
    end
    cla; hold on;
    % between-subject error bars
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(BLBeta,1));
    curData = squeeze(BLBeta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .12],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .15],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .2],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(4,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .3],'linewidth', 2}, 'patchSaturation', .1);
    xlim([-200 1000]); xlabel('Time (ms); resp-locked')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral mu-beta relative to probe')
    
%% Figure: topography +- 100ms peri-response (avg. across loads)
    
cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-2*10^-6 2*10^-6];

figure;
plotData = [];
plotData.label = dataResponseAvg_AlphaBeta{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
idxTime = dataResponseAvg_AlphaBeta{1,1}.time>-100 & dataResponseAvg_AlphaBeta{1,1}.time<100;
% average across loads
LoadAverageL = squeeze(nanmean(cat(4, dataResponseAvg_AlphaBeta_BL{1,1}, dataResponseAvg_AlphaBeta_BL{2,1}, ...
    dataResponseAvg_AlphaBeta_BL{3,1}, dataResponseAvg_AlphaBeta_BL{4,1}),4));
plotData.powspctrm = squeeze(nanmean(nanmean(LoadAverageL(:,:,idxTime),3),1))';
ft_topoplotER(cfg,plotData);

%% baseline correction: -200:0 on grand average beta series (condition-average!)
% TO DO: lock to -200 ms prior to stimulus onset

dataProbeAvg_AlphaBeta_BL = []; dataResponseAvg_AlphaBeta_BL = []; dataResponseAvg_AlphaBetaDiff_BL =[];
idxBL = dataResponseAvg_AlphaBeta{1,1}.time >-200 & dataResponseAvg_AlphaBeta{1,1}.time<200;
for indAge = 1%:2
    % rearrange data in matrix
    for indCond = 1:4
        BetaProbe(indCond,:,:,:) = dataProbeAvg_AlphaBeta{indCond,indAge}.individual;
        BetaProbeDiff(indCond,:,:,:) = dataProbeAvg_AlphaBetaDiff{indCond,indAge}.individual;
        BetaResp(indCond,:,:,:) = dataResponseAvg_AlphaBeta{indCond,indAge}.individual;
    end
    for indCond = 1:4
        dataProbeAvg_AlphaBeta_BL{indCond,indAge} = dataProbeAvg_AlphaBeta{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(nanmean(BetaProbe(:,:,:,idxBL),4),1)),1,1,numel(idxBL));

        dataResponseAvg_AlphaBeta_BL{indCond,indAge} = dataResponseAvg_AlphaBeta{indCond,indAge}.individual-...
             repmat(squeeze(nanmean(nanmean(BetaProbe(:,:,:,idxBL),4),1)),1,1,numel(idxBL));
        
        dataResponseAvg_AlphaBetaDiff_BL{indCond,indAge} = dataResponseAvg_AlphaBetaDiff{indCond,indAge}.individual-...
             repmat(squeeze(nanmean(nanmean(BetaProbeDiff(:,:,:,idxBL),4),1)),1,1,numel(idxBL));

    end
end

%% non-baselined plots

figure; 
subplot(2,6,1); imagesc(dataProbeAvg{1,1}.time, [], squeeze(nanmean(dataProbeAvg{1,1}.individual,1)));
subplot(2,6,3); imagesc(dataResponseAvg{1,1}.time, [], squeeze(nanmean(dataResponseAvg{1,1}.individual,1)));
subplot(2,6,5); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBeta{1,1}.individual,1)));
subplot(2,6,6); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBeta{4,1}.individual,1)));
subplot(2,6,6+1); imagesc(dataProbeAvg{1,1}.time, [], squeeze(nanmean(dataProbeAvgDiff{1,1}.individual,1)));
subplot(2,6,6+3); imagesc(dataResponseAvg{1,1}.time, [], squeeze(nanmean(dataResponseAvgDiff{1,1}.individual,1)));
subplot(2,6,6+5); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{1,1}.individual,1)));
subplot(2,6,6+6); imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{4,1}.individual,1)));

figure; imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{4,1}.individual,1))-...
    squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{1,1}.individual,1)));

figure; imagesc(dataResponseAvg_AlphaBeta{1,1}.time, [], squeeze(nanmean(dataResponseAvg_AlphaBeta{4,1}.individual,1))-...
    squeeze(nanmean(dataResponseAvg_AlphaBeta{1,1}.individual,1)));
    
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataProbeAvg_AlphaBeta{indCond,1}.individual(:,[27,36],:),2)),4));
        plot(dataProbeAvg_AlphaBeta{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral mu-beta relative to probe')
subplot(2,2,2); hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataResponseAvg_AlphaBeta{indCond,1}.individual(:,[27,36],:),2)),4));
        plot(dataResponseAvg_AlphaBeta{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-800 200]); xlabel('Time (ms); resp-locked')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral mu-beta relative to response')

%% plot contralateral readiness potential/ motor execution potential

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataProbeAvg{indCond,1}.individual(:,[27,36],:),2)),4));
        plot(dataProbeAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral ERP relative to probe')
subplot(2,2,2); cla; hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataResponseAvg{indCond,1}.individual(:,[27,36],:),2)),4));
        plot(dataProbeAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-800 800]); xlabel('Time (ms); resp-locked')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral ERP relative to response')
% subplot(2,2,3); hold on;
%     for indCond = 1:4
%         curData = squeeze(nanmean(cat(4, nanmean(dataProbeAvg{indCond,2}.individual(:,[27,31],:),2)),4));
%         plot(dataProbeAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
%     end
%     xlim([-200 1000]); xlabel('Time (ms); probe-locked')
%     legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
%     line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
%     title('OA: Contralateral mu-beta relative to probe')
% subplot(2,2,4); cla; hold on;
%     for indCond = 1:4
%         curData = squeeze(nanmean(cat(4, nanmean(dataResponseAvg{indCond,2}.individual(:,[27,31],:),2)),4));
%         plot(dataProbeAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
%     end
%     xlim([-800 800]); xlabel('Time (ms); resp-locked')
%     line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
%     title('OA: Contralateral mu-beta relative to response')

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataProbeAvgDiff{indCond,1}.individual(:,[27,36],:),2)),4));
        plot(dataProbeAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral ERP relative to probe')
subplot(2,2,2); cla; hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataResponseAvgDiff{indCond,1}.individual(:,[27,36],:),2)),4));
        plot(dataResponseAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-800 800]); xlabel('Time (ms); resp-locked')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral ERP relative to response')

figure;
cfg.zlim = [-8*10^-5 8*10^-5];
plotData = [];
plotData.label = dataResponseAvg_AlphaBeta{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
idxTime = dataResponseAvg_AlphaBeta{1,1}.time>-50 & dataResponseAvg_AlphaBeta{1,1}.time<0;
% average across loads
LoadAverageL = squeeze(nanmean(cat(4, dataResponseAvgDiff{1,1}.individual, dataResponseAvgDiff{2,1}.individual, ...
    dataResponseAvgDiff{3,1}.individual, dataResponseAvgDiff{4,1}.individual),4));
plotData.powspctrm = squeeze(nanmean(nanmean(LoadAverageL(:,:,idxTime),3),1))';
ft_topoplotER(cfg,plotData);

%% baseline correction: -200:0 on grand average LRPs (condition-specific!)

dataResponseAvgDiff_BL = []; dataProbeAvgDiff_BL = [];
idxBL = dataProbeAvgDiff{1,1}.time >-200 & dataProbeAvgDiff{1,1}.time<0;
for indAge = 1%:2
    for indCond = 1:4
        dataProbeAvgDiff_BL{indCond,indAge} = dataProbeAvgDiff{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataProbeAvgDiff{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numel(idxBL));
        dataResponseAvgDiff_BL{indCond,indAge} = dataResponseAvgDiff{indCond,indAge}.individual-...
            repmat(squeeze(nanmean(dataProbeAvgDiff{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numel(idxBL));
    end
end

figure;
cfg.zlim = [];
plotData = [];
plotData.label = dataResponseAvg_AlphaBeta{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
idxTime = dataResponseAvg_AlphaBeta{1,1}.time>-50 & dataResponseAvg_AlphaBeta{1,1}.time<0;
% average across loads
LoadAverageL = squeeze(nanmean(cat(4, dataResponseAvgDiff_BL{1,1}, dataResponseAvgDiff_BL{2,1}, ...
    dataResponseAvgDiff_BL{3,1}, dataResponseAvgDiff_BL{4,1}),4));
plotData.powspctrm = squeeze(nanmean(nanmean(LoadAverageL(:,:,idxTime),3),1))';
ft_topoplotER(cfg,plotData);

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataProbeAvgDiff_BL{indCond,1}(:,[27,36],:),2)),4));
        plot(dataProbeAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral ERP relative to probe')
subplot(2,2,2); cla; hold on;
    for indCond = 1:4
        curData = squeeze(nanmean(cat(4, nanmean(dataResponseAvgDiff_BL{indCond,1}(:,[27,36],:),2)),4));
        plot(dataResponseAvg{1,1}.time,squeeze(nanmean(curData,1)), 'LineWidth', 2)
    end
    xlim([-800 800]); xlabel('Time (ms); resp-locked')
    line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    title('YA: Contralateral ERP relative to response')